#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

import parser

TABLENAME = 'logentry'

class LogDB(object):
    def __init__(self, dbname):
        self.cnx = sqlite3.connect(dbname)
        self.crs = self.cnx.cursor()
        db_clean(self.crs)
        db_init(self.crs)
        self.cnx.commit()

    def fill(self, logfilename):
        db_fill(self.crs, logfilename)
        self.cnx.commit()

    def search(self, **kwargs):
        return db_search(self.crs, **kwargs)

    def close(self):
        self.crs.close()
        self.cnx.close()


def db_clean(crs):
    crs.execute('DROP TABLE logentry;')

def db_init(crs):
    crs.execute('''CREATE TABLE logentry (
        ip VARCHAR(15) NOT NULL,
        clientid TEXT,
        user TEXT,
        datetime TEXT,
        verb VARCHAR(4) NOT NULL,
        resp_code INT,
        resp_length INT,
        referrer TEXT,
        useragent TEXT,
        req_length INT
    );''')

def db_fill(crs, logfilename):
    crs.executemany('''
        INSERT INTO logentry
                (ip, clientid, user, datetime, verb,
                resp_code, resp_length, referrer, useragent, req_length)
            VALUES (:ip, :clientid, :user, :datetime, :verb,
                    :resp_code, :resp_length, :referrer, :useragent, :req_length)
        ;''', parser.apache_log_generator(logfilename))


def db_search(crs, **kwargs):
    cond = ' AND '.join('%s="%s"' % (k,v)
                        for k,v in kwargs.items())
    res = crs.execute('SELECT * FROM logentry '
                      'WHERE %s;' % cond)
    for line in res.fetchall():
        yield line

if __name__ == "__main__":
    db = LogDB('logs.db')
    db.fill('access.log')
    gen = db.search(ip='18.1.6.14', verb='GET')
    print [res for res in gen]
    db.close()
