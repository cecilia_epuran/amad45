#!/usr/bin/env python
# -*- coding: utf-8 -*-

from BaseHTTPServer import HTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler
import urlparse

from db import LogDB


class MyHTTPServer(HTTPServer):

    def __init__(self, dbname, logfilename, *args, **kwargs):
        HTTPServer.__init__(self, *args, **kwargs)
        self.db = LogDB(dbname)
        self.db.fill(logfilename)


class GetHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        q = urlparse.urlparse(self.path)
        kwargs = dict(urlparse.parse_qsl(q.query))
        gen = self.server.db.search(**kwargs)
        self.send_response(200)
        self.end_headers()
        for line in gen:
            self.wfile.write(str(line) + '\n')
        return

if __name__ == '__main__':
    server = MyHTTPServer('logs.db', 'access.log',
                          ('localhost', 8080),
                          GetHandler)
    print 'Starting server, use <Ctrl-C> to stop'
    server.serve_forever()
