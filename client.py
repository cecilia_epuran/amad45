#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib2

params = {'ip': '18.1.6.14', 'verb': 'GET'}
url_params = {
    'SERVER': 'localhost',
    'PORT': 8080,
    'search_params': '&'.join('%s=%s' % (k,v)
                              for (k,v) in params.iteritems())
}
url = 'http://%(SERVER)s:%(PORT)d/search?%(search_params)s'
stream = urllib2.urlopen(url % url_params)
response = stream.read()
print response
