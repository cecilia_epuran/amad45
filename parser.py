#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
import re
import json

class LogEntry(object):

    def __init__(self, **kwargs):
        for k, v in kwargs.iteritems():
            if v == "-":
                v = None
            if k == 'datetime':
                v = datetime.strptime(v.split()[0],
                                          '%d/%b/%Y:%H:%M:%S')
            setattr(self, k, v)
        assert self.verb in ('GET', 'POST'), "unknown verb"

    def match(self, **kwargs):
        for k, v in kwargs.iteritems():
            if not hasattr(self, k):
                return False
            if getattr(self, k) != v:
                return False
        return True

    def __str__(self):
        return "LogEntry at timestamp %s" % self.datetime


class ApacheLogIterator(object):
    """inheriting from object, so __iter__ and next are required
    """
    def __init__(self, filename):
        self.file = open(filename)

    def __iter__(self):
        return self

    def next(self):
        """StopIteration is called implicitly by file.next"""
        line = self.file.next()
        return parse_alog_line(line)

    def __del__(self):
        """Closing the file descriptor when the ApacheLogIterator object
        is destroyed"""
        self.file.close()


class ApacheLogFileIterator(file):
    """inheriting drectly from file, so only next needs to be overloaded
    """
    def next(self):
        """super(ApacheLogFileIterator, self) does not work because old-style class
        """
        line = file.next(self)
        return parse_alog_line(line)


def timestamp(original_parser):
    def modified_parser(line):
        print datetime.now().isoformat()
        return original_parser(line)

    return modified_parser


# @timestamp
def parse_alog_line(line):
    rex = (
'^(?P<ip>\d+\.\d+\.\d+\.\d+)'
' (?P<clientid>\S+)'
' (?P<user>\S+)'
' \[(?P<datetime>.*)\]'
' \"(?P<verb>.*) /.*\"'
' (?P<resp_code>\d+)'
' (?P<resp_length>\S+)'          # could be '-'
' \"(?P<referrer>.*)\"'
' \"(?P<useragent>.*)\"'
' (?P<req_length>\d+)')
    params = re.match(rex, line).groupdict()
    return params
    # return LogEntry(**params)


def apache_log_generator(filename):
    fd = open(filename)
    try:
        for line in fd:
            yield parse_alog_line(line)
    except GeneratorExit:
        print 'early stop'
    finally:
        fd.close()


class LogJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return str(obj)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)

def global_to_json(entry):
    return json.dumps(entry.__dict__, cls=LogJSONEncoder)

# monkeypatch LogEntry
LogEntry.to_json = global_to_json


if __name__ == "__main__":
    g = apache_log_generator('access.log')
    for entry in g:
        if entry.user == 'dave':
            g.close()
    # for entry in ApacheLogIterator('access.log'):
    #     print entry
    # for entry in ApacheLogFileIterator('access.log'):
    #     print entry

